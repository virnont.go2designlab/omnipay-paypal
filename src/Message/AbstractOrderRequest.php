<?php

namespace Omnipay\PayPal\Message;

class AbstractOrderRequest extends AbstractRequest
{
    /**
     * Set the payment intent.
     *
     * @param string $intent
     */
    public function setIntent($intent)
    {
        $this->setParameter('intent', $intent);
    }

    /**
     * Get the payment intent.
     *
     * @return string
     */
    public function getIntent()
    {
        return $this->getParameter('intent');
    }

    /**
     * Set shipping preference.
     *
     * @param string $preference
     */
    public function setShippingPreference($preference)
    {
        $this->setParameter('shipping_preference', $preference);
    }

    /**
     * Get shipping preference.
     *
     * @return string
     */
    public function getShippingPreference()
    {
        return $this->getParameter('shipping_preference');
    }

    public function getEndpoint()
    {
        return parent::getEndpoint() . '/checkout/orders';
    }

    public function getData()
    { }
}
