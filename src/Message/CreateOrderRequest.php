<?php

namespace Omnipay\PayPal\Message;

class CreateOrderRequest extends AbstractOrderRequest
{
    public function getData()
    {
        $this->validate('intent', 'amount');

        return [
            'intent' => $this->getIntent(),
            'application_context' => [
                'return_url'          => $this->getReturnUrl(),
                'cancel_url'          => $this->getCancelUrl(),
                'shipping_preference' => $this->getShippingPreference(),
            ],
            'purchase_units' => [
                [
                    'reference_id'  => $this->getTransactionId(),
                    'amount' => [
                        'value'         => $this->getAmount(),
                        'currency_code' => $this->getCurrency(),
                    ],
                    'description'   => $this->getDescription(),
                ],
            ],
        ];
    }

    protected function createResponse($data, $statusCode)
    {
        return $this->response = new CreateOrderResponse($this, $data, $statusCode);
    }
}
