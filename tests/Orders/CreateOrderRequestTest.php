<?php

namespace Tests\Omnipay\PayPal\Orders;

use Omnipay\Tests\TestCase;
use Omnipay\PayPal\Message\CreateOrderRequest;

class CreateOrderRequestTest extends TestCase
{
    /**
     * @var CreateOrderRequest
     */
    private $request;

    public function setUp()
    {
        parent::setUp();

        $client = $this->getHttpClient();
        $request = $this->getHttpRequest();
        $this->request = new CreateOrderRequest($client, $request);

        $this->request->initialize([
            'intent' => 'AUTHORIZE',
            'amount' => 1000,
        ]);
    }

    public function testGetData()
    {
        $data = $this->request->getData();
        $this->assertEquals('AUTHORIZE', $data['intent']);
        // $this->assertEquals(1000, $data['purchase_units']['amount']['value']);
    }

    public function testEndpoint()
    {
        $this->assertStringEndsWith('/checkout/orders', $this->request->getEndpoint());
    }
}
